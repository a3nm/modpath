# Survey of results on the ModPath and ModCycle problems

This is a survey note, which has not been submitted to a peer-reviewed
publication venue. The stable version of the note is available [on
arXiv](https://arxiv.org/abs/2409.00770); this repository contains the current
version of the note, which may be more recent.

You can download the current version of the note compiled from this repository
in [PDF](https://a3nm.gitlab.io/modpath/main.pdf).

The article is licensed under the Creative Commons "Attribution 4.0
International" CC BY 4.0 license. To view a copy of this license, visit
<https://creativecommons.org/licenses/by/4.0/>.

